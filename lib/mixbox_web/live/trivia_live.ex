defmodule MixboxWeb.TriviaLive do
  use MixboxWeb, :live_view

  alias Mixbox.Trivia

  @impl true
  def mount(_params, _session, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_params(%{"id" => id}, _, socket) do
    question_index = 1

    {:noreply,
     socket
     |> assign(:page_title, "Trivia Game")
     |> assign(:game, Trivia.get_game_with_players(id))
     |> assign(:question_index, question_index)
     |> assign(:question, Trivia.question(question_index))}
  end


end
