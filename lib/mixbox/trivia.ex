defmodule Mixbox.Trivia do
  @moduledoc """
  The Trivia context.
  """

  import Ecto.Query, warn: false
  alias Mixbox.Repo

  alias Mixbox.Trivia.Game
  alias Mixbox.Trivia.Player
  alias Mixbox.Trivia.Question

  @doc """
  Returns the list of games.

  ## Examples

      iex> list_games()
      [%Game{}, ...]

  """
  def list_games do
    Repo.all(Game)
  end

  @doc """
  Gets a single game.

  Raises `Ecto.NoResultsError` if the Game does not exist.

  ## Examples

      iex> get_game!(123)
      %Game{}

      iex> get_game!(456)
      ** (Ecto.NoResultsError)

  """
  def get_game!(id), do: Repo.get!(Game, id)

  def get_game_with_players(id) do
    Repo.one(
      from game in Game,
        where: game.id == ^id,
        preload: [:players]
    )
  end

  @doc """
  Creates a game.

  ## Examples

      iex> create_game(%{field: value})
      {:ok, %Game{}}

      iex> create_game(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_game(attrs \\ %{}) do
    %Game{}
    |> Game.changeset(attrs)
    |> Repo.insert()
  end

  def start_game do
    %Game{}
    |> Game.changeset(%{code: generate_game_code()})
    |> Repo.insert()
  end

  def run_game(game) do
    update_game(game, %{status: "running"})
  end

  def players_for_game(game_id) do
    Repo.all(
      from p in Player,
        where: p.game_id == ^game_id
    )
  end

  def find_game_by_code(code), do: Repo.get_by(Game, code: code)

  def player_for_game_with_name(game_id, name) do
    Repo.get_by(Player, game_id: game_id, name: name)
  end

  def add_player_to_game(game_id, player_name) do
    if player = player_for_game_with_name(game_id, player_name) do
      player
    else
      {:ok, player} =
        %Player{}
        |> Player.changeset(%{game_id: game_id, name: player_name})
        |> Repo.insert()

      player
    end
  end

  def question(index) do
    Repo.one(from q in Question, where: q.index == ^index, preload: :answers)
  end

  @doc """
  Updates a game.

  ## Examples

      iex> update_game(game, %{field: new_value})
      {:ok, %Game{}}

      iex> update_game(game, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_game(%Game{} = game, attrs) do
    game
    |> Game.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a game.

  ## Examples

      iex> delete_game(game)
      {:ok, %Game{}}

      iex> delete_game(game)
      {:error, %Ecto.Changeset{}}

  """
  def delete_game(%Game{} = game) do
    Repo.delete(game)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking game changes.

  ## Examples

      iex> change_game(game)
      %Ecto.Changeset{data: %Game{}}

  """
  def change_game(%Game{} = game, attrs \\ %{}) do
    Game.changeset(game, attrs)
  end

  def generate_game_code() do
    random_string(4) |> String.upcase()
  end

  defp random_string(length) do
    :crypto.strong_rand_bytes(length) |> Base.url_encode64() |> binary_part(0, length)
  end
end
