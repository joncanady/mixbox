defmodule Mixbox.Repo.Migrations.AddCorrectFlagToAnswer do
  use Ecto.Migration

  def change do
    alter table(:answers) do
      add :correct, :boolean
    end
  end
end
