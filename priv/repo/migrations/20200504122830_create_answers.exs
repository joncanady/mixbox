defmodule Mixbox.Repo.Migrations.CreateAnswers do
  use Ecto.Migration

  def change do
    create table(:answers) do
      add :question_id, references(:questions)
      add :answer_text, :string

      timestamps()
    end
  end
end
