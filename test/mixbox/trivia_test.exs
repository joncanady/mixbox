defmodule Mixbox.TriviaTest do
  use Mixbox.DataCase

  alias Mixbox.Repo
  alias Mixbox.Trivia
  alias Mixbox.Trivia.Game
  alias Mixbox.Trivia.Player
  import Ecto.Query, warn: false

  def player_count() do
    Repo.one(from p in Player, select: count())
  end

  describe "start_game/0" do
    test "start_game/0 creates a game with the default status" do
      assert {:ok, %Game{} = game} = Trivia.start_game()
      assert game.status == "waiting_for_players"
    end

    test "start_game/0 creates different codes for different games" do
      {:ok, %Game{} = game1} = Trivia.start_game()
      {:ok, %Game{} = game2} = Trivia.start_game()

      assert game1.code != game2.code
    end
  end

  describe "add_player_to_game/2" do
    test "creates a new Player in the database" do
      original_player_count = player_count()
      game = insert(:game)

      Trivia.add_player_to_game(game.id, "Jon")

      assert player_count() == original_player_count + 1
    end

    test "returns the existing user with that name" do
      game = insert(:game)

      player1 = Trivia.add_player_to_game(game.id, "Jon")
      original_player_count = player_count()

      player2 = Trivia.add_player_to_game(game.id, "Jon")

      assert player_count() == original_player_count
      assert player1 == player2
    end
  end

  describe "run_game/1" do
    test "changes the status of the game to `running`" do
      game = insert(:game)
      {:ok, game} = Trivia.run_game(game)
      assert(game.status == "running")
    end
  end

  describe "question/1" do
    test "returns a question" do
      question1 = insert(:question, index: 1)
      question2 = insert(:question, index: 2)

      assert(question1 == Trivia.question(1))
    end
  end
end
